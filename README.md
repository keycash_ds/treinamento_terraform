# Install Terraform & AWS CLI - SETUP

- [Tutorial de instalação Windows](https://www.youtube.com/watch?v=6FxVEJ8zess)
- [Tutorial de instalação Linux](https://www.youtube.com/watch?v=XILtrogBrl4)

## Summary
- Install Terraform CLI
- Install Hashicorp Terraform Plugin
- Install AWS CLI

## Step 1: Install Terraform
### 1.1 MAC or Linux
- [Download Terraform](https://www.terraform.io/downloads.html) 
- [Install Terraform CLI](https://learn.hashicorp.com/tutorials/terraform/install-cli)
- Unzip the package
    ```sh
    cd ~/Downloads/
    unzip <terraform_filename.zip>
    ```
- Move the Terraform Binary to `/usr/local/bin`
    ```sh
    mv ~/Downloads/terraform /usr/local/bin/
    ```
- Verify that Terraform was installed running `terraform -help` in a new terminal session

### 1.2: Windows

- [Download Terraform](https://www.terraform.io/downloads.html) 
- [Install Terraform CLI](https://learn.hashicorp.com/tutorials/terraform/install-cli)
- Unzip the package
- Move the Terraform Binary to a new folder `C:\terraform`
- Search for Environment variable in System Properties
- Find the "path" environment variable and add a new variable with the name of the directory where you installed the Terraform binary. In this case `C:\terraform`
- Verify that Terraform was installed running `terraform -help` in a new PowerShell window.

### 1.3: Install Hashicorp Terraform Plugin for VS Code
- [Install Plugin](https://marketplace.visualstudio.com/items?itemName=HashiCorp.terraform)

## Step 2: Install AWS CLI
- [AWS Install](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
### 2.1 MAC
```sh
# Install AWS CLI V2
curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
sudo installer -pkg AWSCLIV2.pkg -target /
which aws
aws --version
```

### 2.2 Linux
```sh
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
aws --version
```

### 2.3 Windows
- Download and run the AWS CLI MSI installer for Windows (64-bit):
    - [AWS CLI Installer](https://awscli.amazonaws.com/AWSCLIV2.msi)
- Open a terminal and run `aws --version` command