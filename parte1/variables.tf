variable "bucket_name" {
  description = "My Workshop Bucket Name"
  type        = string
  default     = "sandbox-mateus-moura-bucket"
}
