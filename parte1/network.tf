
# ========= Security Group =========
resource "aws_security_group" "workshop_security_group" {
  name        = "sandbox-mateus-moura-sg"
  description = "Trafego aberto via porta 80"

  ingress {
    description      = "HTTP ACCES"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }


  tags = merge(local.common_tags, { "Name" = "sandbox-mateus-moura-sg" })
}