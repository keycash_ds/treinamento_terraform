# ========= S3 Bucket =========
resource "aws_s3_bucket" "workshop_bucket" {
  bucket = var.bucket_name
  acl    = "public-read"

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = merge(local.common_tags, { "Name" = "sandbox-mateus-moura-bucket" })

}

# ========= S3 Bucket Object =========
resource "aws_s3_bucket_object" "html_object" {
  bucket       = aws_s3_bucket.workshop_bucket.id
  key          = "index.html"
  source       = "../arquivos/index.html"
  content_type = "text/html"
  acl          = "public-read"
}

# ========= SQS Queue =========
resource "aws_sqs_queue" "workshop_queue" {
  name                       = "sandbox-mateus-moura-queue"
  delay_seconds              = 2
  visibility_timeout_seconds = 20
  receive_wait_time_seconds  = 20
  sqs_managed_sse_enabled    = true

  tags = merge(local.common_tags, { "Name" = "sandbox-mateus-moura-queue" })
}

# ========= EC2 Instance =========
resource "aws_instance" "workshop_instance" {
  ami             = "ami-04902260ca3d33422" # Linux
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.workshop_security_group.name]

  user_data = <<EOF
        #!/bin/bash
        # Use this for your user data (script from top to bottom)
        # install httpd (Linux 2 version)
        yum update -y
        yum install -y httpd
        systemctl start httpd
        systemctl enable httpd
        echo "<h1>Hello World from $(hostname -f)</h1>" > /var/www/html/index.html
    EOF

  tags = merge(local.common_tags, { "Name" = "sandbox-mateus-moura-instance" })

}

