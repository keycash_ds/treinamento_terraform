# ========= EC2 =========
resource "aws_instance" "workshop_instance" {
    ami                     = "ami-04902260ca3d33422"
    instance_type           = "t2.micro"
    vpc_security_group_ids  = [aws_security_group.workshop_sg.id]
    subnet_id               = aws_subnet.workshop_public_subnet.id

    user_data = <<EOF
        #!/bin/bash
        # Use this for your user data (script from top to bottom)
        # install httpd (Linux 2 version)
        yum update -y
        yum install -y httpd
        systemctl start httpd
        systemctl enable httpd
        echo "<h1>Hello World from $(hostname -f)</h1>" > /var/www/html/index.html   
    EOF

    tags = merge(local.common_tags, {"Name"= "workshop-terraform-instance"})
}