
# ========= VPC =========
resource "aws_vpc" "workshop_vpc" {
    cidr_block = "10.0.0.0/16"

    tags = merge(local.common_tags, {"Name"= "workshop-terraform-vpc"})
}

# ========= Internet Gateway =========
resource "aws_internet_gateway" "workshop_igw" {
    vpc_id = aws_vpc.workshop_vpc.id
    
    tags = merge(local.common_tags, {"Name"= "workshop-terraform-igw"})
}

# ========= Subnet =========
resource "aws_subnet" "workshop_public_subnet" {
    vpc_id                  = aws_vpc.workshop_vpc.id
    cidr_block              = "10.0.1.0/24"
    map_public_ip_on_launch = true

    tags = merge(local.common_tags, {"Name"= "workshop-terraform-subnet-publica"})
}

# ========= Route Table =========
resource "aws_route_table" "workshop_route_table" {
    vpc_id = aws_vpc.workshop_vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.workshop_igw.id
    }

    tags = merge(local.common_tags, {"Name"= "workshop-terraform-route-table"})
}

# ========= Route Table Association =========
resource "aws_route_table_association" "workshop_route_table_association" {
    subnet_id       = aws_subnet.workshop_public_subnet.id
    route_table_id  = aws_route_table.workshop_route_table.id 
}

# ========= Security Group =========
resource "aws_security_group" "workshop_sg" {
    name        = "workshop-terraform-sg"
    description = "Security Goup criado para acessar EC2 do Workshop pt1 via HTTP"
    vpc_id      =  aws_vpc.workshop_vpc.id

    ingress {
        description      = "Acesso a porta 80 - HTTP"
        from_port        = 80
        to_port          = 80
        protocol         = "tcp"
        cidr_blocks      = ["0.0.0.0/0"]
    }

    egress {
        from_port        = 0
        to_port          = 0
        protocol         = "-1"
        cidr_blocks      = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
    }

    tags = merge(local.common_tags, {"Name"= "workshop-terraform-sg"})
}
