# Summary
Nesse arquivo, vou deixar algumas referências e recomendações de materiais a serem estudados antes do treinamento.
Vou falar sobre esses recursos no dia, mas é legal ter um conhecimento prévio dos recursos que vamos criar com o Terraform =)

# Terraform
O Terraform é uma ferramenta de infraestrutura como código, ou seja, podemos declarar e gerenciar os recursos que gostaríamos de criar na AWS (ou outro provedor de computação em nuvem) usando código, fazer a manutenção e o versionamento da mesma. Isso garante uma maior automação para construir a infra, maior confiabilidade e menores chances de erros ao configurar os recursos na AWS. A seguir vou deixar algumas referências do Terraform.

- [Terraform: infraestrutura como código](https://www.youtube.com/watch?v=tE1WZg9ib8k&t)
- [Introduction to Terraform](https://www.terraform.io/intro/index.html)
- [Terraform Documentation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs)
- [Terraform em 10 Minutos](https://www.youtube.com/watch?v=0EAjJe8aPkc)

# Simple Storage Service (S3)
O S3 é um repositório da AWS onde é possível armazenar objetos (imagens, vídeos, arquivos de texto, áudios, etc) em diretórios, chamados de Buckets.
É conhecido como "infinitely scaling" storage, pois não é necessário configurar o "tamanho" ou capacidade de um bucket, ele pode crescer infinitamente.
Além de ser um repositório escalável, o Bucket oferece criptografia, versionamento e políticas de acesso, tornando esse um repositório seguro para o armazenamento de dados.

- Vídeo de introdução ao serviço do S3 da AWS - [S3 Introduction](https://www.youtube.com/watch?v=77lMCiiMilo)

- Demo de como criar um bucket no console, porém vamos criar na prática usando o Terraform - [S3 Demo](https://www.youtube.com/watch?v=e6w9LwZJFIA)


# Simple Queue Service (SQS)
O Amazon SQS é um serviço totalmente gerenciado pela AWS que permite separar aplicações e diminuir a dependência entre elas, dessa forma, se uma aplicação falhar, não vai impactar diretamente na outra.
Para isso, esse serviço recebe mensagens de uma aplicação (essa chamada de producer), armazena na fila e então a aplicação que consome essa mensagem (chamada de consumer) pode acessar a fila, ler o conteúdo da mensagem e por fim deletar a mesma, isso garante que a mensagem não será perdida durante o processo, pois o SQS faz o papel de armazenar as mensagens.

- Vídeo sobre o SQS e Demo - [Simple Queue Service](https://www.youtube.com/watch?v=vLNDaZuA3Dc)


# Amazon Elastic Compute Cloud (EC2)
Esse é um dos serviços mais populares da AWS e funciona como um servidor na nuvem, ou seja, vamos alugar instâncias (máquinas virtuais) para executar nossas aplicações.
Para isso, vamos configurar o sistema operacional da máquina (Windows, Mac OS, Linux), a CPU, memória RAM e o Security Group

- Vídeo sobre a EC2 - [Introduction to EC2](https://www.youtube.com/watch?v=TsRBftzZsQo)


- Demo de como criar uma EC2 no console - [EC2 Demo](https://www.youtube.com/watch?v=iHX-jtKIVNA)


# Security Group
O Security Group funciona como um firewall para as máquinas EC2, com ele é possível controlar o acesso de entrada e saída das EC2s

- Vídeo sobre Security Group - [Security Group](https://www.youtube.com/watch?v=nA3yN76cNxo)


# Virtual Private Cloud (VPC) & Subnets
VPC é uma rede privada na AWS onde podemos colocar recursos como instâncias EC2, Lambdas, Banco de dados, APIs e etc. Por padrão, a AWS já oferece uma VPC, porém podemos criar mais VPCs, a fim de centralizar os recursos e serviços.

As Subnets são segmentos de uma VPC, podendo ser pública ou privada. Toda subnet deve estar ligada à uma zona de disponibilidade, e a vantagem de ter mais de uma Zona de Disponibilidade é fazer o backup das máquinas utilizadas, dos bancos de dados, dos lambdas, a fim de não perder todas as informações caso ocorra algum desastre nos dados. Toda subnet tem um Cidr_Block, que é um intervalo de IPs disponíveis para os recursos dentro da subnet.

- Vídeo sobre VPC - [What is a VPC?](https://www.youtube.com/watch?v=7XnpdZF_COA&t)

- Esse vídeo cobre boa parte da infraestrutura de rede na AWS que vamos criar com o Terraform - [VPC e Subnets](https://www.youtube.com/watch?v=EynJQPxyL64)

### Esse conteúdo é o suficiente para entendermos o que vamos criar na AWS com o Terraform. Além desses, vou comentar sobre o Cloudfront, Route53 e API Gateway, porém não vamos criar esses recursos no dia do treinamento.
